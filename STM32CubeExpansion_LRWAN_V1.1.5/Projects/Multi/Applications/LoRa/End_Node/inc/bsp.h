/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech

Description: contains all hardware driver

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/
 /******************************************************************************
  * @file    bsp.h
  * @author  MCD Application Team
  * @version V1.1.5
  * @date    30-March-2018
  * @brief   contains all hardware driver
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_H__
#define __BSP_H__

#ifdef __cplusplus
 extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include "timeServer.h"
#include "lora.h"
#include "hw.h"
#include "is74_workflow.h"

/* Exported macros -----------------------------------------------------------*/
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA   
#define DM_AUX_TIMER_NMB 3   

#define dmLORAWAN_APP_DATA_BUFF_SIZE                           64
#define dmLORAWAN_APP_MAX_PACKET_SIZE                          46
#define MAX_LORAWAN_PACKET_NMB                                  6
   

#define DoMINO_APP_PORT 2
#define DEFAULT_PACKET_TYPE 0x01
//#define LORAWAN_DEFAULT_CONFIRM_MSG_STATE  LORAWAN_UNCONFIRMED_MSG
#define LORAWAN_DEFAULT_CONFIRM_MSG_STATE           LORAWAN_CONFIRMED_MSG

#define dmLORAWAN_EVENT_DEFAULT                        0x01   
#define dmLORAWAN_EVENT_TIMEOUT                        0x02  
   
#define is74LORAWAN_APP_PORT                            2
#define dmLORAWAN_SERVICE_PORT                          254

#define OUTPUT_ON GPIO_PIN_SET
#define OUTPUT_OFF GPIO_PIN_RESET   
   
#define EEPROM_ADDR_IS74_START 0x08080100  
/* Exported types ------------------------------------------------------------*/
   
typedef enum __dm_main_state
{
  MAIN_STATE_IDLE,
  MAIN_STATE_JOINING,
  MAIN_STATE_MAIN_CYCLE,
  MAIN_STATE_SEND_LORA_MESSAGE,
  
}dm_main_state;   
typedef enum __tx_rx_state
{
  TXRX_BUSY,
  TXRX_OK,
  TXRX_TIMEOUT,
  TXRX_ERROR
}tx_rx_state;
typedef enum __lora_join_state
{
  LORA_JOINST_IDLE,
  LORA_JOINST_BUSY,
  LORA_JOINST_JOINED,
  LORA_JOINST_NOTJOINED  
}lora_join_state;

typedef enum __input_pin_type
{
  INPUT_PIN_DIG_COUNTER,
  INPUT_PIN_DIG_FLAG,
  INPUT_PIN_ANALOG_VALUE,
  INPUT_PIN_ANALOG_TRIGGER,
}input_pin_type;
///////////////////////////////////////////////////////////////////////////////
typedef enum __output_pin_type
{
  OUTPUT_PIN_DIG_OK,

}output_pin_type;
///////////////////////////////////////////////////////////////////////////////
//
typedef struct __input_pin_Variable
{
  GPIO_TypeDef* Port;
  uint16_t Pin;
  input_pin_type Type;
  uint32_t Counter;
  bool PinFlag;
  //for analog type
  uint32_t outputV;
  uint16_t adc_value;

}input_pin_Variable;
/////////////////////////////////////////
typedef struct __output_pin_Variable
{
  GPIO_TypeDef* Port;
  uint16_t Pin;
  output_pin_type Type;
  uint32_t Counter;
  bool PinFlag;
}output_pin_Variable;
/////////////////////////////////////////
typedef struct __dm_main_Variable
{
  dm_main_state state;

  TimerEvent_t dmMainTimer;
  TimerEvent_t dmTxCycleTimer;
  TimerEvent_t dmUnixTimer;
  TimerEvent_t dmAuxTimer[DM_AUX_TIMER_NMB];

  bool dmMainTimerAlarmFlag;
  uint8_t *LoRaTxBuff;
  uint8_t LoRaTxLen;
  uint8_t LoRaTxByteCnt;

  uint8_t LoRaTxPacketType;
  uint8_t LoRaTxPacketNmb;
  uint8_t LoRaTxPacketCnt;
  uint8_t LoRaTxEvent;
}dm_main_Variable;
////////////////////////////////////////////
typedef struct __dm_to_is74_main_Variable
{
  uint8_t State;
  tx_rx_state TxState;
  tx_rx_state RxState;
  uint16_t RxCount;
  uint8_t* LoRaRxBuff;
  uint8_t LoRaRxBuffSize;
  uint32_t UnixTime;
  

  bool TxComplete;
  bool RxComplete;
  bool TimerAlarm;
  bool LoRaTxTimerFlag;
  bool LoRaTxComplete;
  bool LoRaRxCompleteFlag;
  

  lora_join_state LoRaJoinState;
}dm_to_is74_main_Variable;

/* Exported constants --------------------------------------------------------*/
#define USART2_RX_TIMEOUT_VALUE 1000
/* External variables --------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */ 
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart);
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);

void USART2_RxTimeout(void);
void is74_TimerTimeout(void);
void dmMainTimerCallback(void);
void dmTxCycleTimerCallback(void); 
void dmUnixTimerCallback(void);
void dmOutputInit(void);
void dmAPI_OutputSetState(output_pin_Variable *output_pin,GPIO_PinState state);

void dm_SetMainTimer(uint32_t timeout);
void dmLoRaWAN_Send_Packet(void);

//API
void dmAPI_UART_ReInit(void);
void dmAPI_UART_Transaction(uint8_t *TxData, uint16_t TxSize, uint8_t *RxData, uint16_t RxSize, uint16_t RxTimeout);
void dmAPI_SetTimer(uint32_t timeout);
void dm_API_SetLoRaTxTimer(uint32_t timeout);
void dmAPI_LoRaWAN_StartJoin();
//void dmAPI_LoRaWAN_Send(uint8_t *buff,uint16_t len);
void dmAPI_LoRaWAN_Send(uint8_t *buff,uint16_t len, uint8_t PktType, uint8_t TxEvent );

void dm_API_StartUnixTimer(uint32_t StartValue);
void dm_API_SaveToEEPROM(uint32_t addres, uint32_t *buff,uint8_t len);
void dm_API_ReastoreFromEEPROM(uint32_t addres, uint32_t *buff,uint8_t len);







#ifdef __cplusplus
}
#endif

#endif /* __BSP_H__ */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
