/******************************************************************************
  * @file    main.c
  * @author  MCD Application Team
  * @version V1.1.5
  * @date    30-March-2018
  * @brief   this is the main!
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "hw.h"
#include "low_power_manager.h"
#include "lora.h"
#include "bsp.h"
#include "timeServer.h"
#include "vcom.h"
#include "version.h"
#include "is74_workflow.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/*!
 * CAYENNE_LPP is myDevices Application server.
 */
//#define CAYENNE_LPP
#define LPP_DATATYPE_DIGITAL_INPUT  0x0
#define LPP_DATATYPE_DIGITAL_OUTPUT 0x1
#define LPP_DATATYPE_HUMIDITY       0x68
#define LPP_DATATYPE_TEMPERATURE    0x67
#define LPP_DATATYPE_BAROMETER      0x73
#define LPP_APP_PORT 99
/*!
 * Defines the application data transmission duty cycle. 5s, value in [ms].
 */
#define APP_TX_DUTYCYCLE                            10000
/*!
 * LoRaWAN Adaptive Data Rate
 * @note Please note that when ADR is enabled the end-device should be static
 */
#define LORAWAN_ADR_STATE LORAWAN_ADR_ON
/*!
 * LoRaWAN Default data Rate Data Rate
 * @note Please note that LORAWAN_DEFAULT_DATA_RATE is used only when ADR is disabled 
 */
#define LORAWAN_DEFAULT_DATA_RATE DR_0
/*!
 * LoRaWAN application port
 * @note do not use 224. It is reserved for certification
 */
//#define LORAWAN_APP_PORT                            2
/*!
 * LoRaWAN default endNode class port
 */
#define LORAWAN_DEFAULT_CLASS                       CLASS_C
/*!
 * LoRaWAN default confirm state
 */
//#define LORAWAN_DEFAULT_CONFIRM_MSG_STATE           LORAWAN_UNCONFIRMED_MSG
/*!
 * User application data buffer size
 */
#define LORAWAN_APP_DATA_BUFF_SIZE                           64
/*!
 * User application data
 */
//static uint8_t AppDataBuff[LORAWAN_APP_DATA_BUFF_SIZE];

/*!
 * User application data structure
 */
//lora_AppData_t AppData={ AppDataBuff,  0 ,0 };
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/* call back when LoRa endNode has received a frame*/
static void LORA_RxData( lora_AppData_t *AppData);

/* call back when LoRa endNode has just joined*/
static void LORA_HasJoined( void );

/* call back when LoRa endNode has just switch the class*/
static void LORA_ConfirmClass ( DeviceClass_t Class );

/* call back when server needs endNode to send a frame*/
static void LORA_TxNeeded ( void );
	
/* LoRa endNode send request*/
static void Send( void );

/* start the tx process*/
static void LoraStartTx(TxEventType_t EventType);

/* tx timer callback function*/
static void OnTxTimerEvent( void );

is74_UART_Init_Variable is74_InitParam;

/* Private variables ---------------------------------------------------------*/
extern uint32_t LoRaMacState;
UART_HandleTypeDef huart2;
dm_to_is74_main_Variable dm_to_is74_main_var;
dm_main_Variable dmMainVar;

/* load Main call backs structure*/
static LoRaMainCallback_t LoRaMainCallbacks = { HW_GetBatteryLevel,
                                                HW_GetTemperatureLevel,
                                                HW_GetUniqueId,
                                                HW_GetRandomSeed,
                                                LORA_RxData,
                                                LORA_HasJoined,
                                                LORA_ConfirmClass,
                                                LORA_TxNeeded};

/*!
 * Specifies the state of the application LED
 */
//static uint8_t AppLedStateOn = RESET;
/////////////////////////////////////////////
//Timer Declaration                                               
TimerEvent_t TxTimer;
TimerEvent_t Usart2_RxTimer;
TimerEvent_t is74_Timer;
/////////////////////////////////////////////

/* !
 *Initialises the Lora Parameters
 */
static  LoRaParam_t LoRaParamInit= {LORAWAN_ADR_STATE,
                                    LORAWAN_DEFAULT_DATA_RATE,  
                                    LORAWAN_PUBLIC_NETWORK};

/* Private functions ---------------------------------------------------------*/
extern lora_AppData_t dmAppData;
/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
//uint32_t TempBuff[10];
  int main( void )
{
  /* STM32 HAL library initialization*/
  HAL_Init();
  
  /* Configure the system clock*/
  SystemClock_Config();
  
  /* Configure the hardware*/
  HW_Init();
  
  /* USER CODE BEGIN 1 */
  dmOutputInit();
  is74__UART_Init();
  dmAPI_UART_ReInit();

  HAL_FLASH_Unlock();

  /* USER CODE END 1 */
  
  /*Disbale Stand-by mode*/
  LPM_SetOffMode(LPM_APPLI_Id , LPM_Disable );
  
  /* Configure the Lora Stack*/
  LORA_Init( &LoRaMainCallbacks, &LoRaParamInit);
#ifdef RU868
  PRINTF("Friqency Plan: RU864\n\r");  
#else 
  PRINTF("Friqency Plan: EU868\n\r");  
#endif  
  PRINTF("VERSION: %X\n\r", VERSION);
  uint8_t ver = Radio.Read(0x42);
  if (ver==0x22) PRINTF("Radio Chipset SX1272\n\r");
   else if (ver==0x12) PRINTF("Radio Chipset SX1276\n\r");
    else  PRINTF("Radio Chipset Not Detected\n\r");  

  //LORA_Join();
  
  //LoraStartTx( TX_ON_TIMER) ;
  //for (uint8_t s=0;s<10;s++) TempBuff[s]=s;
  ////////////////////////////////////////////////
  //TimerInit
  TimerInit( &Usart2_RxTimer, USART2_RxTimeout );
  TimerInit( &is74_Timer, is74_TimerTimeout );
  TimerInit( &dmMainVar.dmMainTimer, dmMainTimerCallback );
  TimerInit( &dmMainVar.dmTxCycleTimer, dmTxCycleTimerCallback );
  TimerInit( &dmMainVar.dmUnixTimer, dmUnixTimerCallback );

  ////////////////////////////////////////////////  
  is74__OneTime();
  
  while( 1 )
  {
    DISABLE_IRQ( );
    /* if an interrupt has occurred after DISABLE_IRQ, it is kept pending 
     * and cortex will not enter low power anyway  */

#ifndef LOW_POWER_DISABLE
    LPM_EnterLowPower( );
#endif

    ENABLE_IRQ();
    
    /* USER CODE BEGIN 2 */
   is74_mainloop();
    switch (dmMainVar.state)
    {
    case MAIN_STATE_IDLE:
      {
        break;
      }
    case MAIN_STATE_JOINING:
      {
        if (dmMainVar.dmMainTimerAlarmFlag)
        {
          if ( LORA_JoinStatus () != LORA_SET)
          {
            /*Not joined, try again later*/
            dmMainVar.state = MAIN_STATE_IDLE;
            //dm_to_is74_main_var.LoRaTxComplete = false;
            dm_to_is74_main_var.LoRaJoinState = LORA_JOINST_NOTJOINED;
            break;
          }
          else 
          {
            dmMainVar.state = MAIN_STATE_MAIN_CYCLE;
            dm_to_is74_main_var.LoRaJoinState = LORA_JOINST_JOINED;
          }

        }
        if ( LORA_JoinStatus () == LORA_SET)
        {
            dmMainVar.state = MAIN_STATE_MAIN_CYCLE;
            dm_to_is74_main_var.LoRaJoinState = LORA_JOINST_JOINED;
           // dm_API_SetLoRaTxTimer (20000);
            
            break;          
        }
          
        break;
      }  
    case MAIN_STATE_MAIN_CYCLE:
      {
        /*if (dm_to_is74_main_var.LoRaTxTimerFlag)
        {
            dm_to_is74_main_var.LoRaTxTimerFlag = false;
            dmAPI_LoRaWAN_Send(TempBuff,5);
            dm_API_SetLoRaTxTimer (20000);

        }*/

        break;
      }      
    case MAIN_STATE_SEND_LORA_MESSAGE:
      {
        if (!LoRaMacState) 
        {
          if (dmMainVar.LoRaTxPacketCnt == dmMainVar.LoRaTxPacketNmb)
          {
            dm_to_is74_main_var.LoRaTxComplete = true;
            dmMainVar.state = MAIN_STATE_MAIN_CYCLE;
            break;
          }
          else
          {
            dmMainVar.LoRaTxPacketCnt++;
            HAL_Delay(2000);
            dmLoRaWAN_Send_Packet();
          }
        }
        break;
      }            
    default:break;
    }
    
    
    /* USER CODE END 2 */
  }
}
static void LORA_HasJoined( void )
{
#if( OVER_THE_AIR_ACTIVATION != 0 )
  PRINTF("JOINED\n\r");
#endif
  LORA_RequestClass( LORAWAN_DEFAULT_CLASS );
}

static void Send( void )
{

//  LORA_send( &AppData, LORAWAN_DEFAULT_CONFIRM_MSG_STATE);
  
  /* USER CODE END 3 */
}


static void LORA_RxData( lora_AppData_t *AppData )
{
  /* USER CODE BEGIN 4 */
  PRINTF("PACKET RECEIVED ON PORT %d\n\r", AppData->Port);

  switch (AppData->Port)
  {
    case 3:
    /*this port switches the class*/
    if( AppData->BuffSize == 1 )
    {
      switch (  AppData->Buff[0] )
      {
        case 0:
        {
          LORA_RequestClass(CLASS_A);
          break;
        }
        case 1:
        {
          LORA_RequestClass(CLASS_B);
          break;
        }
        case 2:
        {
          LORA_RequestClass(CLASS_C);
          break;
        }
        default:
          break;
      }
    }
    break;
    case is74LORAWAN_APP_PORT:
      {
      dm_to_is74_main_var.LoRaRxCompleteFlag = true;
      dm_to_is74_main_var.LoRaRxBuff = AppData->Buff;
      dm_to_is74_main_var.LoRaRxBuffSize = AppData->BuffSize;
      break;
    }

    case dmLORAWAN_SERVICE_PORT:
  {

    break;
  }
  default:
    break;
  }
  /* USER CODE END 4 */
}





static void LORA_ConfirmClass ( DeviceClass_t Class )
{
  PRINTF("switch to class %c done\n\r","ABC"[Class] );

  /*Optionnal*/
  /*informs the server that switch has occurred ASAP*/
  //AppData.BuffSize = 0;
  //AppData.Port = LORAWAN_APP_PORT;
  
  //LORA_send( &AppData, LORAWAN_UNCONFIRMED_MSG);*/
}

static void LORA_TxNeeded ( void )
{
 // AppData.BuffSize = 0;
//  AppData.Port = LORAWAN_APP_PORT;
  
 // LORA_send( &AppData, LORAWAN_UNCONFIRMED_MSG);
}

#ifdef USE_B_L072Z_LRWAN1
static void OnTimerLedEvent( void )
{
  LED_Off( LED_RED1 ) ; 
}
#endif
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
