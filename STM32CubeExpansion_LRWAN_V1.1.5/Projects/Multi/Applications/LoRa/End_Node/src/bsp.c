 /******************************************************************************
  * @file    bsp.c
  * @author  MCD Application Team
  * @version V1.1.5
  * @date    30-March-2018
  * @brief   manages the sensors on the application
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
  /* Includes ------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include "hw.h"
#include "timeServer.h"
#include "bsp.h"
#include "is74_workflow.h"



/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern UART_HandleTypeDef huart2;
extern is74_UART_Init_Variable is74_InitParam;
extern dm_to_is74_main_Variable dm_to_is74_main_var;
extern dm_main_Variable dmMainVar;
extern TimerEvent_t Usart2_RxTimer;
extern TimerEvent_t is74_Timer;
extern uint8_t UART2_TX_buff[128];
extern uint8_t UART2_RX_buff[128];

input_pin_Variable ext_pin_1 = {GPIOH,GPIO_PIN_0,INPUT_PIN_DIG_COUNTER,0,0,0};
input_pin_Variable ext_pin_2 = {GPIOH,GPIO_PIN_1,INPUT_PIN_DIG_COUNTER,0,0,0};
input_pin_Variable ext_pin_3 = {GPIOC,GPIO_PIN_6,INPUT_PIN_DIG_COUNTER,0,0,0};
input_pin_Variable ext_pin_4 = {GPIOC,GPIO_PIN_7,INPUT_PIN_DIG_COUNTER,0,0,0};

output_pin_Variable output_ok_1 = {GPIOB,GPIO_PIN_15,OUTPUT_PIN_DIG_OK,0,0};
output_pin_Variable output_ok_2 = {GPIOB,GPIO_PIN_14,OUTPUT_PIN_DIG_OK,0,0};
output_pin_Variable output_ok_3 = {GPIOB,GPIO_PIN_13,OUTPUT_PIN_DIG_OK,0,0};
output_pin_Variable output_ok_4 = {GPIOB,GPIO_PIN_12,OUTPUT_PIN_DIG_OK,0,0};

/* Private function prototypes -----------------------------------------------*/
/* Exported functions ---------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
   dm_to_is74_main_var.TxComplete = true;
   dm_to_is74_main_var.TxState = TXRX_OK;
}
////////////////////////////////////////////////////////////////////////////////
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
   dm_to_is74_main_var.RxComplete = true;
   dm_to_is74_main_var.RxState = TXRX_OK;
   dm_to_is74_main_var.RxCount = huart2.RxXferSize;
}
////////////////////////////////////////////////////////////////////////////////
void USART2_RxTimeout(void)
{
   
   dm_to_is74_main_var.RxComplete = true;
   dm_to_is74_main_var.RxState = TXRX_TIMEOUT;
   dm_to_is74_main_var.RxCount = huart2.RxXferSize-huart2.RxXferCount;
   HAL_UART_AbortReceive_IT(&huart2);
}
////////////////////////////////////////////////////////////////////////////////
void is74_TimerTimeout(void)
{
   dm_to_is74_main_var.TimerAlarm = true;
}
////////////////////////////////////////////////////////////////////////////////
void dmMainTimerCallback(void)
{
   dmMainVar.dmMainTimerAlarmFlag = true;
}   
////////////////////////////////////////////////////////////////////////////////
void dmTxCycleTimerCallback(void) 
{
  dm_to_is74_main_var.LoRaTxTimerFlag = true;
}
///////////////////////////////////////////////////////////////////////////////
void dmUnixTimerCallback(void)
{
  dm_to_is74_main_var.UnixTime++;
  TimerSetValue( &dmMainVar.dmUnixTimer,  1000);   
  TimerStart( &dmMainVar.dmUnixTimer);  
}
////////////////////////////////////////////////////////////////////////////////   
void dmOutputInit(void)
{
  GPIO_InitTypeDef initStruct={0};
  //OUTPUT
  
  dmAPI_OutputSetState(&output_ok_1,OUTPUT_OFF);
  dmAPI_OutputSetState(&output_ok_2,OUTPUT_OFF);
  dmAPI_OutputSetState(&output_ok_3,OUTPUT_OFF);
  dmAPI_OutputSetState(&output_ok_4,OUTPUT_OFF);
  
  initStruct.Mode = GPIO_MODE_OUTPUT_PP;
  initStruct.Pull = GPIO_NOPULL;
  initStruct.Speed = GPIO_SPEED_HIGH;
  HW_GPIO_Init( output_ok_1.Port, output_ok_1.Pin, &initStruct );
  HW_GPIO_Init( output_ok_2.Port, output_ok_2.Pin, &initStruct );
  HW_GPIO_Init( output_ok_3.Port, output_ok_3.Pin, &initStruct );
  HW_GPIO_Init( output_ok_4.Port, output_ok_4.Pin, &initStruct );
}
////////////////////////////////////////////////////////////////////////////////




/* Private variables ---------------------------------------------------------*/
uint8_t dmAppDataBuff[dmLORAWAN_APP_DATA_BUFF_SIZE];
lora_AppData_t dmAppData ={ dmAppDataBuff,  0 ,0 };

////////////////////////////////////////////////////////////////////////////////
//DoMINO Group API function


void dmAPI_OutputSetState(output_pin_Variable *output_pin,GPIO_PinState state)
{
  HW_GPIO_Write(output_pin->Port,output_pin->Pin,state);
  if  ( state == OUTPUT_ON) output_pin->PinFlag = true;
  else output_pin->PinFlag = false;
}
/*---------------------------------------------------------------------------*/
void dmAPI_UART_ReInit(void)
{
  huart2.Instance = USART2;
  huart2.Init.BaudRate = is74_InitParam.BaudRate;
  huart2.Init.WordLength = is74_InitParam.WordLength;
  huart2.Init.StopBits = is74_InitParam.StopBits;
  huart2.Init.Parity = is74_InitParam.Parity;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_RS485Ex_Init(&huart2, UART_DE_POLARITY_HIGH, 0, 0) != HAL_OK)
  {
    Error_Handler();
  }

}
////////////////////////////////////////////////////////////////////////////////
void dmAPI_UART_Transaction(uint8_t *TxData, uint16_t TxSize, uint8_t *RxData, uint16_t RxSize, uint16_t RxTimeout)
{
  dm_to_is74_main_var.RxComplete = false;
  dm_to_is74_main_var.TxComplete = false;
  dm_to_is74_main_var.RxState = TXRX_BUSY;
  dm_to_is74_main_var.TxState = TXRX_BUSY;
  dm_to_is74_main_var.RxCount = 0;
  TimerSetValue( &Usart2_RxTimer,  RxTimeout);   
  TimerStart( &Usart2_RxTimer);
  HAL_UART_Transmit_IT(&huart2, TxData,TxSize);
  HAL_UART_Receive_IT(&huart2,RxData,RxSize);
}
////////////////////////////////////////////////////////////////////////////////
void dmAPI_LoRaWAN_StartJoin()
{
  LORA_Join();
  dm_to_is74_main_var.LoRaJoinState = LORA_JOINST_BUSY;
  dmMainVar.state = MAIN_STATE_JOINING;
  dm_SetMainTimer(20000);
}
////////////////////////////////////////////////////////////////////////////////
/*void dmAPI_LoRaWAN_Send(uint8_t *buff,uint16_t len)
{ 
  //this is new send start
  dm_to_is74_main_var.LoRaTxComplete = false;
  dm_to_is74_main_var.LoRaRxCompleteFlag = false;
  dmMainVar.LoRaTxBuff = buff;
  dmMainVar.LoRaTxLen = len;
  dmMainVar.LoRaTxPacketNmb = (dmMainVar.LoRaTxLen/dmLORAWAN_APP_MAX_PACKET_SIZE)+1;
  if (dmMainVar.LoRaTxPacketNmb > MAX_LORAWAN_PACKET_NMB) dmMainVar.LoRaTxPacketNmb = MAX_LORAWAN_PACKET_NMB;
  dmMainVar.LoRaTxPacketCnt = 1;
  dmMainVar.LoRaTxByteCnt = 0;
  dmMainVar.LoRaTxEvent = dmLORAWAN_EVENT_DEFAULT;
  dmMainVar.LoRaTxPacketType = DEFAULT_PACKET_TYPE;
  dmLoRaWAN_Send_Packet();
}*/
///////////////////////////////////////////////////////////////////////////////////////////////
void dmAPI_LoRaWAN_Send(uint8_t *buff,uint16_t len, uint8_t PktType, uint8_t TxEvent )
{ 
  //this is new send start
  dm_to_is74_main_var.LoRaTxComplete = false;
  dm_to_is74_main_var.LoRaRxCompleteFlag = false;
  dmMainVar.LoRaTxBuff = buff;
  dmMainVar.LoRaTxLen = len;
  dmMainVar.LoRaTxPacketNmb = (dmMainVar.LoRaTxLen/dmLORAWAN_APP_MAX_PACKET_SIZE)+1;
  if (dmMainVar.LoRaTxPacketNmb > MAX_LORAWAN_PACKET_NMB) dmMainVar.LoRaTxPacketNmb = MAX_LORAWAN_PACKET_NMB;
  dmMainVar.LoRaTxPacketCnt = 1;
  dmMainVar.LoRaTxByteCnt = 0;
dmMainVar.LoRaTxPacketType = PktType;  
 dmMainVar.LoRaTxEvent = TxEvent;
// dmMainVar.LoRaTxEvent = dmLORAWAN_EVENT_DEFAULT;
// dmMainVar.LoRaTxPacketType = DEFAULT_PACKET_TYPE;
  dmLoRaWAN_Send_Packet();
}
////////////////////////////////////////////////////////////////////////////////
void dmLoRaWAN_Send_Packet(void)
{
  uint8_t i=0,j;
  if (dmMainVar.LoRaTxPacketCnt == dmMainVar.LoRaTxPacketNmb) dmAppData.BuffSize = 5+(dmMainVar.LoRaTxLen - ((dmMainVar.LoRaTxPacketCnt-1)*dmLORAWAN_APP_MAX_PACKET_SIZE));
    else dmAppData.BuffSize = 5+dmLORAWAN_APP_MAX_PACKET_SIZE;
  dmAppData.Port = DoMINO_APP_PORT;
  dmAppData.Buff[i++] = dmMainVar.LoRaTxPacketType;
  dmAppData.Buff[i++] = HW_GetTemperatureLevel()>>8;
  dmAppData.Buff[i++] = dmMainVar.LoRaTxPacketNmb;
  dmAppData.Buff[i++] = dmMainVar.LoRaTxPacketCnt;
  dmAppData.Buff[i++] = dmMainVar.LoRaTxEvent;
  j=(dmMainVar.LoRaTxPacketCnt-1)*dmLORAWAN_APP_MAX_PACKET_SIZE;
  do{

  dmAppData.Buff[i++] =dmMainVar.LoRaTxBuff[j];
    j++;

  }
  while (i!=dmAppData.BuffSize);
  LORA_send( &dmAppData, LORAWAN_DEFAULT_CONFIRM_MSG_STATE); 
  dmMainVar.state = MAIN_STATE_SEND_LORA_MESSAGE;
}
////////////////////////////////////////////////////////////////////////////////
void dmAPI_SetTimer(uint32_t timeout)
{
  TimerSetValue( &is74_Timer,  timeout);   
  TimerStart( &is74_Timer);
}
////////////////////////////////////////////////////////////////////////////////
void dm_API_SetLoRaTxTimer(uint32_t timeout)
{
  TimerSetValue( &dmMainVar.dmTxCycleTimer,  timeout);   
  TimerStart( &dmMainVar.dmTxCycleTimer);
  dm_to_is74_main_var.LoRaTxTimerFlag = false;

}
////////////////////////////////////////////////////////////////////////////////
void dm_API_StartUnixTimer(uint32_t StartValue)
{
  dm_to_is74_main_var.UnixTime = StartValue;
  TimerSetValue( &dmMainVar.dmUnixTimer,  1000);   
  TimerStart( &dmMainVar.dmUnixTimer);
}
////////////////////////////////////////////////////////////////////////////////
void dm_API_SaveToEEPROM(uint32_t addres, uint32_t *buff, uint8_t len)
{
  for(uint8_t a=0;a<len;a++) *(__IO uint32_t*)(EEPROM_ADDR_IS74_START+(addres*4)+(a*4)) = buff[a];

}
////////////////////////////////////////////////////////////////////////////////
void dm_API_ReastoreFromEEPROM(uint32_t addres, uint32_t *buff,uint8_t len)
{
  for(uint8_t a=0;a<len;a++)  buff[a] = *(__IO uint32_t*)(EEPROM_ADDR_IS74_START+(addres*4)+(a*4));

}
////////////////////////////////////////////////////////////////////////////////
void dm_SetMainTimer(uint32_t timeout)
{
  TimerSetValue( &dmMainVar.dmMainTimer,  timeout);   
  TimerStart( &dmMainVar.dmMainTimer);
}




/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
